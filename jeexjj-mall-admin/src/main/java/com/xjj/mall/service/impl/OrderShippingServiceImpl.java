/****************************************************
 * Description: ServiceImpl for t_mall_order_shipping
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.OrderShippingEntity;
import com.xjj.mall.dao.OrderShippingDao;
import com.xjj.mall.service.OrderShippingService;

@Service
public class OrderShippingServiceImpl extends XjjServiceSupport<OrderShippingEntity> implements OrderShippingService {

	@Autowired
	private OrderShippingDao orderShippingDao;

	@Override
	public XjjDAO<OrderShippingEntity> getDao() {
		
		return orderShippingDao;
	}
}