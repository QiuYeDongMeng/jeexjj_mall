<#--
/****************************************************
 * Description: t_mall_address的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/address/save" id=tabId>
   <input type="hidden" name="id" value="${address.id}"/>
   
   <@formgroup title='address_id'>
	<input type="text" name="addressId" value="${address.addressId}" check-type="required number">
   </@formgroup>
   <@formgroup title='user_id'>
	<input type="text" name="userId" value="${address.userId}" check-type="number">
   </@formgroup>
   <@formgroup title='user_name'>
	<input type="text" name="userName" value="${address.userName}" >
   </@formgroup>
   <@formgroup title='tel'>
	<input type="text" name="tel" value="${address.tel}" >
   </@formgroup>
   <@formgroup title='street_name'>
	<input type="text" name="streetName" value="${address.streetName}" >
   </@formgroup>
   <@formgroup title='is_default'>
	<input type="text" name="isDefault" value="${address.isDefault}" check-type="number">
   </@formgroup>
</@input>